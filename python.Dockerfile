# General purpose development image, set up with python and poetry, zsh.
ARG PYVER=3
FROM python:${PYVER}

# Setup OS
ENV HOME=/home/worker \
    POETRY_VIRTUALENVS_CREATE=false \
    EDITOR='vim'
RUN addgroup --system --gid 10001 worker 
RUN adduser -u 10000 --system --group worker --home $HOME
RUN apt-get update && apt-get -y install \
    zsh git curl vim less gcc
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
RUN git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Setup Poetry
RUN pip3 install poetry